XNAT EDF Client
===============

An EDF viewer with XNAT integration. Successor to [Copla Editor](https://github.com/somnonetz/copla-editor)

Build
-----

* Create a file `.env` and add the following:

```
REACT_APP_XNAT_API_URL=<xnat_site_url>/REST
```

* Build the application:

```sh
export NODE_OPTIONS=--openssl-legacy-provider
npm install
npm run build
```
